/**
 * @file driver.h
 */

#ifndef DRIVER_H
#define DRIVER_H

typedef struct comms_ctx
{
    int fds[2];
} comms_ctx_t;

typedef struct thread_ctx
{
    int sock_fd;
} thread_ctx_t;

void setup_pollfds(struct pollfd fds[], comms_ctx_t ctx[], int size);
void *callers(void *arg);
bool setup_socketpairs(comms_ctx_t ctx[], int size);
void destroy_socketpairs(comms_ctx_t ctx[], int size);
#endif /* DRIVER_H */