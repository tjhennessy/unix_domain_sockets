/**
 * @file driver.c
 * 
 * A simple UNIX domain socket program that incorporates pthreads.
 * Main thread spins up N threads and polls awaiting POLLIN revents
 * from these threads.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <poll.h>
#include <stdbool.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <driver.h>

#define NUM_THREADS 100
#define SLEEP_RANGE 3 // in seconds
#define THREAD_SOCK_FD 0
#define MAIN_SOCK_FD   1

static bool run = true;
static size_t buf_size;

static void sig_handler(int _)
{
    (void)_;
    run = false;
}

static void free_string(char **buf)
{
    free(*buf);
}

void *callers(void *arg)
{
    uint32_t ms_con = 1000000;
    uint32_t sec;
    uint32_t num_sec_range = SLEEP_RANGE;
    ssize_t num_bytes;
    char buf[] = "Hello from thread\n";
    thread_ctx_t *ctx = (thread_ctx_t *) arg;

    buf_size = sizeof(buf);

    while (run)
    {
        sec = random() % num_sec_range;
        usleep(sec * ms_con);

        num_bytes = write(ctx->sock_fd, buf, buf_size);
        if (num_bytes < 0)
        {
            perror("write");
            goto shutdown;
        }
        else if (num_bytes != sizeof(buf))
        {   // Partial writes are possible
            // TODO: implement sendall
            fprintf(stderr, "Error: incorrect num bytes written.\n");
            goto shutdown;
        }
    }

    shutdown:
    pthread_exit(NULL);
}

bool setup_socketpairs(comms_ctx_t ctx[], int size)
{
    int rv;

    for (int i = 0; i < size; i++)
    {
        rv = socketpair(AF_UNIX, SOCK_SEQPACKET, 0, ctx[i].fds);
        if (rv < 0)
        {
            perror("socketpair");
            return false;
        }
    }

    return true;
}

void destroy_socketpairs(comms_ctx_t ctx[], int size)
{
    for (int i = 0; i < size; i++)
    {
        close(ctx[i].fds[MAIN_SOCK_FD]);
        close(ctx[i].fds[THREAD_SOCK_FD]);
    }
}

void setup_pollfds(struct pollfd fds[], comms_ctx_t ctx[], int size)
{
    for (int i = 0; i < size; i++)
    {
        fds[i].fd = ctx[i].fds[MAIN_SOCK_FD];
        fds[i].events = POLLIN;
    }
}

int main(void)
{
    bool s;
    int rv;
    int ready_count;
    int nfds;
    char *buf __attribute__((__cleanup__(free_string)));
    struct pollfd pollfds[NUM_THREADS];
    comms_ctx_t ctx[NUM_THREADS];
    pthread_t tids[NUM_THREADS];

    signal(SIGINT, sig_handler);

    buf = malloc(buf_size * sizeof(char));
    if (NULL == buf)
    {
        fprintf(stderr, "Error: malloc failed.\n");
        return 1;
    }

    s = setup_socketpairs(ctx, NUM_THREADS);
    if (s == false)
    {
        fprintf(stderr, "Error: could not set up socketpairs.\n");
        return 1;
    }

    nfds = NUM_THREADS;
    setup_pollfds(pollfds, ctx, nfds);

    for (int i = 0; i < NUM_THREADS; i++)
    {
        rv = pthread_create(&tids[i], NULL, callers, (void *) &ctx[i]);
        if (rv != 0)
        {
            fprintf(stderr, "Error: could not create thread.\n");
            return 1;
        }
    }

    while (run)
    {
        ready_count = poll(pollfds, nfds, -1);
        if (ready_count < 0)
        {
            perror("poll");
            break;
        }
        else
        {
            for (int i = 0; i < ready_count; i++)
            {
                if (pollfds[i].revents & POLLIN)
                {
                    rv = read(pollfds[i].fd, buf, buf_size);
                    printf("%s", buf);
                    memset(buf, 0, sizeof(buf_size));
                }
            }
        }
    }

    for (int i = 0; i < NUM_THREADS; i++)
    {
        pthread_join(tids[i], NULL);
    }

    destroy_socketpairs(ctx, NUM_THREADS);

    pthread_exit(NULL);
}