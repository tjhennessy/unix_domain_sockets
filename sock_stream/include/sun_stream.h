/**
 * @file sun_stream.h
 */

#ifndef SUN_STREAM_H
#define SUN_STREAM_H
#include <sys/un.h>
#include <sys/socket.h>
#include <errno.h>

#define SV_SOCK_PATH "/tmp/sun_sock_xfr" /* Should use path which is secure dir */

#define BUF_SIZE 100

#endif /* SUN_STREAM_H */