/**
 * @file sun_stream_client.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sun_stream.h>

int main(void)
{
    struct sockaddr_un addr;
    int sfd;
    int rv;
    ssize_t num_read;
    char buf[BUF_SIZE];

    sfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (-1 == sfd)
    {
        perror("call to socket failed");
        return 1;
    }

    /* Construct server address, and make the connection */
    
    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, SV_SOCK_PATH, sizeof(addr.sun_path) - 1);

    rv = connect(sfd, (struct sockaddr *) &addr, sizeof(struct sockaddr_un));
    if (-1 == rv)
    {
        fprintf(stderr, "Error: call to connect failed.\n");
        return 1;
    }

    /* Copy stdin to socket */

    while ((num_read = read(STDIN_FILENO, buf, BUF_SIZE)) > 0)
    {
        if (write(sfd, buf, num_read) != num_read)
        {
            fprintf(stderr, "Error: call to write failed.\n");
            return 1;
        }
    }

    if (-1 == num_read)
    {
        fprintf(stderr, "Error: call to read failed.\n");
        return 1;
    }

    return 0;
}