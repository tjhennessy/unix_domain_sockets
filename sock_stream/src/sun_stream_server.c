/**
 * @file sun_stream_server.c
 * 
 * Client connects to server and transfers data via stdin to server.
 * Iterative server accepts client connections and transfers data
 * via stdout.
 */ 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sun_stream.h>

#define BACKLOG 8

void err(char *msg)
{
    fprintf(stderr, "Error: %s\n", msg);
    exit(EXIT_FAILURE);
}

void close_fd(int *fd)
{
    if (*fd != -1)
    {
        close(*fd);
    }
}

int main(void)
{
    struct sockaddr_un addr;
    int sfd __attribute__ ((__cleanup__(close_fd))) = -1;
    int cfd __attribute__ ((__cleanup__(close_fd))) = -1;
    int rv;
    ssize_t num_read;
    ssize_t num_write;
    char buf[BUF_SIZE] = {0};

    sfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (-1 == sfd)
    {
        err("call to socket failed");
    }

    /* Construct server socket address, bind socket to it, and make this a
       listening socket. 
    */
    
    if (strlen(SV_SOCK_PATH) > sizeof(addr.sun_path) - 1)
    {
        err("server socket path length too long");
    }

    /* Ensure path does not already exist  */
    rv = remove(SV_SOCK_PATH);
    if (-1 == rv && errno != ENONET)
    {
        perror("remove");
        err("remove returned an error");
    }
    

    /* Zeroing out entire structure improves portability by
       not focusing on specific implementation details which can differ
       between implementations.  
    */
    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr.sun_family = AF_UNIX;
    /* The below strncpy is guaranteed to be NULL terminated because
       the entire structure was zeroed out and we don't allow the last
       byte to be copied over.
    */
    strncpy(addr.sun_path, SV_SOCK_PATH, sizeof(struct sockaddr_un) - 1);

    rv = bind(sfd, (struct sockaddr *) &addr, sizeof(struct sockaddr_un));
    if (rv < 0)
    {
        err("call to bind failed");
    }

    rv = listen(sfd, BACKLOG);
    if (rv < 0)
    {
        err("call to listen failed");
    }

    for (;;)
    {    
         /* Accept new connection.  The new connection is returned on a new
            socket and stored in cfd.  The listening socket stays open and
            listens for new connections.
         */ 
        cfd = accept(sfd, NULL, NULL);
        if (cfd < 0)
        {
            err("could not accept new client connection");
        }

        /* Transfer data from connected socket to stdout until EOF */
        while ((num_read = read(cfd, buf, BUF_SIZE)) > 0)
        {
            printf("num_read = %ld\n", num_read);
            num_write = write(STDOUT_FILENO, buf, num_read);
            printf("num_write = %ld\n", num_write);
            if (num_write != num_read)
            {
                err("call to write failed");
            }
        }

        if (-1 == num_read)
        {
            err("call to read failed");
        }
    }

    return 0;
}