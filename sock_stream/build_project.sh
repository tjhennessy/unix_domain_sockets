if [ -d "build/" ]; then
    rm -rf build/
fi

# If on the first time program is run and the file DNE
# the program will fail because file DNE, so create it
if [ ! -f "/tmp/sun_sock_xfr" ]; then
    touch "/tmp/sun_sock_xfr"
fi

cmake -H. -Bbuild -DCMAKE_BUILD_TYPE=Debug

cd build/ 

make -j 8